var fs = require('fs');
var readline = require('readline');

// Creating interface to read the file
var lineReader = readline.createInterface({
  input: fs.createReadStream('final.txt')
});

var lastChar, contentToWrite;
var stream = fs.createWriteStream('result.txt', {'flags': 'w'});
// use {'flags': 'a'} to append and {'flags': 'w'} to erase and write a new file

lineReader.on('line', function (line) {
  // Getting the lastchar on the line
  lastChar = line[line.length-1];

  // If lastChar is 0 or 1 we need to write the line adding the break line at the End
  // Else we need to add a separator (just a space) and append the new line
  contentToWrite = (lastChar == '0' || lastChar == '1') ? (line + '\n') : (' ' + line) ;

  stream.write(contentToWrite);
});

lineReader.on('close', function () {
  // Close the file stream
  stream.end();
  console.log('End of process. File successfully generated');
});
